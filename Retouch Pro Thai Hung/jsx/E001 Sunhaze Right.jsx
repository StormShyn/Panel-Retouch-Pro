﻿#target photoshop
//
// SunhazeR_ight.jsx
//

//
// Generated Wed Jul 31 2019 00:48:44 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== Sunhaze R⁬ight ==============
//
$._ext_E001={
run : function SunhazeR_ight() {
  // Make
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putClass(sTID("contentLayer"));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    var desc3 = new ActionDescriptor();
    desc3.putUnitDouble(cTID('Angl'), cTID('#Ang'), -171.25);
    desc3.putEnumerated(cTID('Type'), cTID('GrdT'), cTID('Rdl '));
    desc3.putUnitDouble(cTID('Scl '), cTID('#Prc'), 41);
    var desc4 = new ActionDescriptor();
    desc4.putString(cTID('Nm  '), "Custom");
    desc4.putEnumerated(cTID('GrdF'), cTID('GrdF'), cTID('CstS'));
    desc4.putDouble(cTID('Intr'), 4096);
    var list1 = new ActionList();
    var desc5 = new ActionDescriptor();
    var desc6 = new ActionDescriptor();
    desc6.putDouble(cTID('Rd  '), 255);
    desc6.putDouble(cTID('Grn '), 253.996124267578);
    desc6.putDouble(cTID('Bl  '), 249.000091552734);
    desc5.putObject(cTID('Clr '), sTID("RGBColor"), desc6);
    desc5.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc5.putInteger(cTID('Lctn'), 0);
    desc5.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc5);
    var desc7 = new ActionDescriptor();
    var desc8 = new ActionDescriptor();
    desc8.putDouble(cTID('Rd  '), 255);
    desc8.putDouble(cTID('Grn '), 253.996124267578);
    desc8.putDouble(cTID('Bl  '), 187.001037597656);
    desc7.putObject(cTID('Clr '), sTID("RGBColor"), desc8);
    desc7.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc7.putInteger(cTID('Lctn'), 623);
    desc7.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc7);
    var desc9 = new ActionDescriptor();
    var desc10 = new ActionDescriptor();
    desc10.putDouble(cTID('Rd  '), 0);
    desc10.putDouble(cTID('Grn '), 0);
    desc10.putDouble(cTID('Bl  '), 0);
    desc9.putObject(cTID('Clr '), sTID("RGBColor"), desc10);
    desc9.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc9.putInteger(cTID('Lctn'), 4096);
    desc9.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc9);
    desc4.putList(cTID('Clrs'), list1);
    var list2 = new ActionList();
    var desc11 = new ActionDescriptor();
    desc11.putUnitDouble(cTID('Opct'), cTID('#Prc'), 100);
    desc11.putInteger(cTID('Lctn'), 0);
    desc11.putInteger(cTID('Mdpn'), 50);
    list2.putObject(cTID('TrnS'), desc11);
    var desc12 = new ActionDescriptor();
    desc12.putUnitDouble(cTID('Opct'), cTID('#Prc'), 100);
    desc12.putInteger(cTID('Lctn'), 612);
    desc12.putInteger(cTID('Mdpn'), 50);
    list2.putObject(cTID('TrnS'), desc12);
    var desc13 = new ActionDescriptor();
    desc13.putUnitDouble(cTID('Opct'), cTID('#Prc'), 0);
    desc13.putInteger(cTID('Lctn'), 4096);
    desc13.putInteger(cTID('Mdpn'), 50);
    list2.putObject(cTID('TrnS'), desc13);
    desc4.putList(cTID('Trns'), list2);
    desc3.putObject(cTID('Grad'), cTID('Grdn'), desc4);
    desc2.putObject(cTID('Type'), sTID("gradientLayer"), desc3);
    desc1.putObject(cTID('Usng'), sTID("contentLayer"), desc2);
    executeAction(cTID('Mk  '), desc1, dialogMode);
  };

  // Set
  function step2(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(sTID("contentLayer"), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putBoolean(cTID('Dthr'), true);
    desc2.putUnitDouble(cTID('Angl'), cTID('#Ang'), -79.7);
    desc2.putEnumerated(cTID('Type'), cTID('GrdT'), cTID('Lnr '));
    desc2.putUnitDouble(cTID('Scl '), cTID('#Prc'), 80);
    var desc3 = new ActionDescriptor();
    desc3.putString(cTID('Nm  '), "Custom");
    desc3.putEnumerated(cTID('GrdF'), cTID('GrdF'), cTID('CstS'));
    desc3.putDouble(cTID('Intr'), 4096);
    var list1 = new ActionList();
    var desc4 = new ActionDescriptor();
    var desc5 = new ActionDescriptor();
    desc5.putDouble(cTID('Rd  '), 253.000000119209);
    desc5.putDouble(cTID('Grn '), 231.673158109188);
    desc5.putDouble(cTID('Bl  '), 190.494159758091);
    desc4.putObject(cTID('Clr '), sTID("RGBColor"), desc5);
    desc4.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc4.putInteger(cTID('Lctn'), 0);
    desc4.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc4);
    var desc6 = new ActionDescriptor();
    var desc7 = new ActionDescriptor();
    desc7.putDouble(cTID('Rd  '), 253.996124267578);
    desc7.putDouble(cTID('Grn '), 159.99755859375);
    desc7.putDouble(cTID('Bl  '), 79.002685546875);
    desc6.putObject(cTID('Clr '), sTID("RGBColor"), desc7);
    desc6.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc6.putInteger(cTID('Lctn'), 913);
    desc6.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc6);
    var desc8 = new ActionDescriptor();
    var desc9 = new ActionDescriptor();
    desc9.putDouble(cTID('Rd  '), 255);
    desc9.putDouble(cTID('Grn '), 164.579763114452);
    desc9.putDouble(cTID('Bl  '), 133.000007271767);
    desc8.putObject(cTID('Clr '), sTID("RGBColor"), desc9);
    desc8.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc8.putInteger(cTID('Lctn'), 1848);
    desc8.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc8);
    var desc10 = new ActionDescriptor();
    var desc11 = new ActionDescriptor();
    desc11.putDouble(cTID('Rd  '), 253.000030517578);
    desc11.putDouble(cTID('Grn '), 185.997161865234);
    desc11.putDouble(cTID('Bl  '), 141.001739501953);
    desc10.putObject(cTID('Clr '), sTID("RGBColor"), desc11);
    desc10.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc10.putInteger(cTID('Lctn'), 4096);
    desc10.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc10);
    desc3.putList(cTID('Clrs'), list1);
    var list2 = new ActionList();
    var desc12 = new ActionDescriptor();
    desc12.putUnitDouble(cTID('Opct'), cTID('#Prc'), 96.8627450980392);
    desc12.putInteger(cTID('Lctn'), 0);
    desc12.putInteger(cTID('Mdpn'), 50);
    list2.putObject(cTID('TrnS'), desc12);
    var desc13 = new ActionDescriptor();
    desc13.putUnitDouble(cTID('Opct'), cTID('#Prc'), 92.9411764705882);
    desc13.putInteger(cTID('Lctn'), 0);
    desc13.putInteger(cTID('Mdpn'), 50);
    list2.putObject(cTID('TrnS'), desc13);
    var desc14 = new ActionDescriptor();
    desc14.putUnitDouble(cTID('Opct'), cTID('#Prc'), 78.8235294117647);
    desc14.putInteger(cTID('Lctn'), 1848);
    desc14.putInteger(cTID('Mdpn'), 50);
    list2.putObject(cTID('TrnS'), desc14);
    var desc15 = new ActionDescriptor();
    desc15.putUnitDouble(cTID('Opct'), cTID('#Prc'), 21.9607843137255);
    desc15.putInteger(cTID('Lctn'), 4096);
    desc15.putInteger(cTID('Mdpn'), 50);
    list2.putObject(cTID('TrnS'), desc15);
    desc3.putList(cTID('Trns'), list2);
    desc2.putObject(cTID('Grad'), cTID('Grdn'), desc3);
    desc1.putObject(cTID('T   '), sTID("gradientLayer"), desc2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Set
  function step3(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(sTID("contentLayer"), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putBoolean(cTID('Dthr'), true);
    desc2.putUnitDouble(cTID('Angl'), cTID('#Ang'), -111.04);
    desc2.putEnumerated(cTID('Type'), cTID('GrdT'), cTID('Lnr '));
    desc2.putUnitDouble(cTID('Scl '), cTID('#Prc'), 80);
    var desc3 = new ActionDescriptor();
    desc3.putString(cTID('Nm  '), "Custom");
    desc3.putEnumerated(cTID('GrdF'), cTID('GrdF'), cTID('CstS'));
    desc3.putDouble(cTID('Intr'), 4096);
    var list1 = new ActionList();
    var desc4 = new ActionDescriptor();
    var desc5 = new ActionDescriptor();
    desc5.putDouble(cTID('Rd  '), 253.000000119209);
    desc5.putDouble(cTID('Grn '), 231.673158109188);
    desc5.putDouble(cTID('Bl  '), 190.494159758091);
    desc4.putObject(cTID('Clr '), sTID("RGBColor"), desc5);
    desc4.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc4.putInteger(cTID('Lctn'), 0);
    desc4.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc4);
    var desc6 = new ActionDescriptor();
    var desc7 = new ActionDescriptor();
    desc7.putDouble(cTID('Rd  '), 253.996124267578);
    desc7.putDouble(cTID('Grn '), 159.99755859375);
    desc7.putDouble(cTID('Bl  '), 79.002685546875);
    desc6.putObject(cTID('Clr '), sTID("RGBColor"), desc7);
    desc6.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc6.putInteger(cTID('Lctn'), 913);
    desc6.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc6);
    var desc8 = new ActionDescriptor();
    var desc9 = new ActionDescriptor();
    desc9.putDouble(cTID('Rd  '), 255);
    desc9.putDouble(cTID('Grn '), 164.579763114452);
    desc9.putDouble(cTID('Bl  '), 133.000007271767);
    desc8.putObject(cTID('Clr '), sTID("RGBColor"), desc9);
    desc8.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc8.putInteger(cTID('Lctn'), 1848);
    desc8.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc8);
    var desc10 = new ActionDescriptor();
    var desc11 = new ActionDescriptor();
    desc11.putDouble(cTID('Rd  '), 253.000030517578);
    desc11.putDouble(cTID('Grn '), 185.997161865234);
    desc11.putDouble(cTID('Bl  '), 141.001739501953);
    desc10.putObject(cTID('Clr '), sTID("RGBColor"), desc11);
    desc10.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc10.putInteger(cTID('Lctn'), 4096);
    desc10.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc10);
    desc3.putList(cTID('Clrs'), list1);
    var list2 = new ActionList();
    var desc12 = new ActionDescriptor();
    desc12.putUnitDouble(cTID('Opct'), cTID('#Prc'), 96.8627450980392);
    desc12.putInteger(cTID('Lctn'), 0);
    desc12.putInteger(cTID('Mdpn'), 50);
    list2.putObject(cTID('TrnS'), desc12);
    var desc13 = new ActionDescriptor();
    desc13.putUnitDouble(cTID('Opct'), cTID('#Prc'), 92.9411764705882);
    desc13.putInteger(cTID('Lctn'), 0);
    desc13.putInteger(cTID('Mdpn'), 50);
    list2.putObject(cTID('TrnS'), desc13);
    var desc14 = new ActionDescriptor();
    desc14.putUnitDouble(cTID('Opct'), cTID('#Prc'), 78.8235294117647);
    desc14.putInteger(cTID('Lctn'), 1848);
    desc14.putInteger(cTID('Mdpn'), 50);
    list2.putObject(cTID('TrnS'), desc14);
    var desc15 = new ActionDescriptor();
    desc15.putUnitDouble(cTID('Opct'), cTID('#Prc'), 21.9607843137255);
    desc15.putInteger(cTID('Lctn'), 4096);
    desc15.putInteger(cTID('Mdpn'), 50);
    list2.putObject(cTID('TrnS'), desc15);
    desc3.putList(cTID('Trns'), list2);
    desc2.putObject(cTID('Grad'), cTID('Grdn'), desc3);
    desc1.putObject(cTID('T   '), sTID("gradientLayer"), desc2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Set
  function step4(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putString(cTID('Nm  '), "Sunhaze Right");
    desc1.putObject(cTID('T   '), cTID('Lyr '), desc2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Set
  function step5(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putEnumerated(cTID('Md  '), cTID('BlnM'), cTID('Scrn'));
    desc1.putObject(cTID('T   '), cTID('Lyr '), desc2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Set
  function step6(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putUnitDouble(cTID('Opct'), cTID('#Prc'), 58);
    desc1.putObject(cTID('T   '), cTID('Lyr '), desc2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Set
  function step7(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putEnumerated(cTID('Clr '), cTID('Clr '), cTID('Orng'));
    desc1.putObject(cTID('T   '), cTID('Lyr '), desc2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  step1();      // Make
  step2();      // Set
  step3();      // Set
  step4();      // Set
  step5();      // Set
  step6();      // Set
  step7();      // Set
},
};



//=========================================
//                    SunhazeR_ight.main
//=========================================
//

//SunhazeR_ight.main = function () {
  //SunhazeR_ight();
//};

//SunhazeR_ight.main();

// EOF

//"SunhazeR_ight.jsx"
// EOF
