#target photoshop
//
// HDR_Lessen_Noise.jsx
//

//
// Generated Wed Aug 14 2019 01:34:20 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== HDR [Lessen] Noise ==============
//
$._ext_HDR010={
run : function HDR_Lessen_Noise() {
  // Hide
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var list1 = new ActionList();
    var ref1 = new ActionReference();
    ref1.putName(cTID('Lyr '), "Add Noise");
    list1.putReference(ref1);
    desc1.putList(cTID('null'), list1);
    executeAction(cTID('Hd  '), desc1, dialogMode);
  };

  step1();      // Hide
},
};



//=========================================
//                    HDR_Lessen_Noise.main
//=========================================
//

//HDR_Lessen_Noise.main = function () {
  //HDR_Lessen_Noise();
//};

//HDR_Lessen_Noise.main();

// EOF

//"HDR_Lessen_Noise.jsx"
// EOF
