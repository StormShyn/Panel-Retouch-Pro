#target photoshop
//
// HDR_Increase_Vibrance.jsx
//

//
// Generated Wed Aug 14 2019 01:33:34 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== HDR [Increase] Vibrance ==============
//
$._ext_HDR006={
run : function HDR_Increase_Vibrance() {
  // Hide
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var list1 = new ActionList();
    var ref1 = new ActionReference();
    ref1.putName(cTID('Lyr '), "Neutral Colour");
    list1.putReference(ref1);
    desc1.putList(cTID('null'), list1);
    executeAction(cTID('Hd  '), desc1, dialogMode);
  };

  step1();      // Hide
},
};



//=========================================
//                    HDR_Increase_Vibrance.main
//=========================================
//

//HDR_Increase_Vibrance.main = function () {
 // HDR_Increase_Vibrance();
//};

//HDR_Increase_Vibrance.main();

// EOF

//"HDR_Increase_Vibrance.jsx"
// EOF
