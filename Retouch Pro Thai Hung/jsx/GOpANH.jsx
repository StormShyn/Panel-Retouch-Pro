#target photoshop
//
// GOMANH.jsx
//

//
// Generated Tue Aug 20 2019 23:51:01 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== GOM ANH ==============
//
$._ext_B01={
run : function GOMANH() {
  // Flatten Image
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    executeAction(sTID('flattenImage'), undefined, dialogMode);
  };

  step1(true, true);      // Flatten Image
},
};



//=========================================
//                    GOMANH.main
//=========================================
//

//GOMANH.main = function () {
 // GOMANH();
//};

//GOMANH.main();

// EOF

//"GOMANH.jsx"
// EOF
