#target photoshop
//
// 004 AutoSkinRetouch.jsx
//

//
// Generated Tue Jul 23 2019 03:26:48 GMT+0700
//

cTID = function (s) { return app.charIDToTypeID(s); };
sTID = function (s) { return app.stringIDToTypeID(s); };

//
//==================== Auto Skin Retouch Action 4 ==============
//
$._ext_004 = {
  run: function AutoSkinRetouchAction4() {
    // Make
    function step1(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      var desc2 = new ActionDescriptor();
      desc2.putString(cTID('Nm  '), "COLOR");
      desc1.putObject(cTID('Nw  '), cTID('Lyr '), desc2);
      desc1.putEnumerated(cTID('Usng'), cTID('ArSl'), cTID('Slct'));
      desc1.putBoolean(cTID('Cpy '), true);
      executeAction(cTID('Mk  '), desc1, dialogMode);
    };

    // Make
    function step2(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      var desc2 = new ActionDescriptor();
      desc2.putString(cTID('Nm  '), "TEXTURE");
      desc2.putEnumerated(cTID('Md  '), cTID('BlnM'), sTID("linearLight"));
      desc1.putObject(cTID('Nw  '), cTID('Lyr '), desc2);
      desc1.putEnumerated(cTID('Usng'), cTID('ArSl'), cTID('Slct'));
      desc1.putBoolean(cTID('Cpy '), true);
      executeAction(cTID('Mk  '), desc1, dialogMode);
    };

    // Hide
    function step3(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      var list1 = new ActionList();
      var ref1 = new ActionReference();
      ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
      list1.putReference(ref1);
      desc1.putList(cTID('null'), list1);
      executeAction(cTID('Hd  '), desc1, dialogMode);
    };

    // Select
    function step4(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      var ref1 = new ActionReference();
      ref1.putName(cTID('Lyr '), "COLOR");
      desc1.putReference(cTID('null'), ref1);
      desc1.putBoolean(cTID('MkVs'), false);
      var list1 = new ActionList();
      list1.putInteger(10);
      desc1.putList(cTID('LyrI'), list1);
      executeAction(cTID('slct'), desc1, dialogMode);
    };

    // Surface Blur
    function step5(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      desc1.putUnitDouble(cTID('Rds '), cTID('#Pxl'), 45);
      desc1.putInteger(cTID('Thsh'), 35);
      executeAction(sTID('surfaceBlur'), desc1, dialogMode);
    };

    // Select
    function step6(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      var ref1 = new ActionReference();
      ref1.putName(cTID('Lyr '), "TEXTURE");
      desc1.putReference(cTID('null'), ref1);
      desc1.putBoolean(cTID('MkVs'), false);
      var list1 = new ActionList();
      list1.putInteger(11);
      desc1.putList(cTID('LyrI'), list1);
      executeAction(cTID('slct'), desc1, dialogMode);
    };

    // Show
    function step7(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      var list1 = new ActionList();
      var ref1 = new ActionReference();
      ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
      list1.putReference(ref1);
      desc1.putList(cTID('null'), list1);
      executeAction(cTID('Shw '), desc1, dialogMode);
    };

    // High Pass
    function step8(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      desc1.putUnitDouble(cTID('Rds '), cTID('#Pxl'), 1.5);
      executeAction(sTID('highPass'), desc1, dialogMode);
    };

    // Select
    function step9(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      var ref1 = new ActionReference();
      ref1.putName(cTID('Lyr '), "COLOR");
      desc1.putReference(cTID('null'), ref1);
      desc1.putEnumerated(sTID("selectionModifier"), sTID("selectionModifierType"), sTID("addToSelectionContinuous"));
      desc1.putBoolean(cTID('MkVs'), false);
      var list1 = new ActionList();
      list1.putInteger(10);
      list1.putInteger(11);
      desc1.putList(cTID('LyrI'), list1);
      executeAction(cTID('slct'), desc1, dialogMode);
    };

    // Make
    function step10(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      var ref1 = new ActionReference();
      ref1.putClass(sTID("layerSection"));
      desc1.putReference(cTID('null'), ref1);
      var ref2 = new ActionReference();
      ref2.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
      desc1.putReference(cTID('From'), ref2);
      var desc2 = new ActionDescriptor();
      desc2.putString(cTID('Nm  '), "Thai Hung KrQ AI 4");
      desc2.putEnumerated(cTID('Clr '), cTID('Clr '), cTID('Orng'));
      desc1.putObject(cTID('Usng'), sTID("layerSection"), desc2);
      desc1.putInteger(sTID("layerSectionStart"), 16);
      desc1.putInteger(sTID("layerSectionEnd"), 17);
      desc1.putString(cTID('Nm  '), "Auto Retouch");
      executeAction(cTID('Mk  '), desc1, dialogMode);
    };

    // Make
    function step11(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      desc1.putClass(cTID('Nw  '), cTID('Chnl'));
      var ref1 = new ActionReference();
      ref1.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Msk '));
      desc1.putReference(cTID('At  '), ref1);
      desc1.putEnumerated(cTID('Usng'), cTID('UsrM'), cTID('HdAl'));
      executeAction(cTID('Mk  '), desc1, dialogMode);
    };

    // Hide
    function step12(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      var list1 = new ActionList();
      var ref1 = new ActionReference();
      ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
      list1.putReference(ref1);
      desc1.putList(cTID('null'), list1);
      executeAction(cTID('Hd  '), desc1, dialogMode);
    };

    // Select
    function step13(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      var ref1 = new ActionReference();
      ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Bckw'));
      desc1.putReference(cTID('null'), ref1);
      desc1.putBoolean(cTID('MkVs'), false);
      var list1 = new ActionList();
      list1.putInteger(1);
      desc1.putList(cTID('LyrI'), list1);
      executeAction(cTID('slct'), desc1, dialogMode);
    };

    // Color Range
    function step14(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      desc1.putInteger(cTID('Fzns'), 1);
      desc1.putEnumerated(cTID('Clrs'), cTID('Clrs'), sTID("skinTone"));
      desc1.putInteger(sTID("colorModel"), 0);
      executeAction(sTID('colorRange'), desc1, dialogMode);
    };

    // Select
    function step15(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      var ref1 = new ActionReference();
      ref1.putName(cTID('Lyr '), "Thai Hung KrQ AI 4");
      desc1.putReference(cTID('null'), ref1);
      desc1.putBoolean(cTID('MkVs'), false);
      executeAction(cTID('slct'), desc1, dialogMode);
    };

    // Show
    function step16(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      var list1 = new ActionList();
      var ref1 = new ActionReference();
      ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
      list1.putReference(ref1);
      desc1.putList(cTID('null'), list1);
      executeAction(cTID('Shw '), desc1, dialogMode);
    };

    // Fill
    function step17(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      desc1.putEnumerated(cTID('Usng'), cTID('FlCn'), cTID('Wht '));
      desc1.putUnitDouble(cTID('Opct'), cTID('#Prc'), 100);
      desc1.putEnumerated(cTID('Md  '), cTID('BlnM'), cTID('Nrml'));
      executeAction(cTID('Fl  '), desc1, dialogMode);
    };

    // Set
    function step18(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      var ref1 = new ActionReference();
      ref1.putProperty(cTID('Chnl'), sTID("selection"));
      desc1.putReference(cTID('null'), ref1);
      desc1.putEnumerated(cTID('T   '), cTID('Ordn'), cTID('None'));
      executeAction(cTID('setd'), desc1, dialogMode);
    };

    // Gaussian Blur
    function step19(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      desc1.putUnitDouble(cTID('Rds '), cTID('#Pxl'), 10);
      executeAction(sTID('gaussianBlur'), desc1, dialogMode);
    };

    // Set
    function step20(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      var ref1 = new ActionReference();
      ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
      desc1.putReference(cTID('null'), ref1);
      var desc2 = new ActionDescriptor();
      desc2.putUnitDouble(cTID('Opct'), cTID('#Prc'), 90);
      desc1.putObject(cTID('T   '), cTID('Lyr '), desc2);
      executeAction(cTID('setd'), desc1, dialogMode);
    };

    // Select
    function step21(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      var ref1 = new ActionReference();
      ref1.putClass(cTID('PbTl'));
      desc1.putReference(cTID('null'), ref1);
      executeAction(cTID('slct'), desc1, dialogMode);
    };

    // Reset
    function step22(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      var ref1 = new ActionReference();
      ref1.putProperty(cTID('Clr '), cTID('Clrs'));
      desc1.putReference(cTID('null'), ref1);
      executeAction(cTID('Rset'), desc1, dialogMode);
    };

    step1();      // Make
    step2();      // Make
    step3();      // Hide
    step4();      // Select
    step5(true, true);      // Surface Blur
    step6();      // Select
    step7();      // Show
    step8(true, true);      // High Pass
    step9();      // Select
    step10();      // Make
    step11();      // Make
    step12();      // Hide
    step13();      // Select
    step14();      // Color Range
    step15();      // Select
    step16();      // Show
    step17();      // Fill
    step18();      // Set
    step19();      // Gaussian Blur
    step20();      // Set
    step21();      // Select
    step22();      // Reset
  },
};



//=========================================
//                    AutoSkinRetouchAction4.main
//=========================================
//

//AutoSkinRetouchAction4.main = function () {
  //AutoSkinRetouchAction4();
//};

//AutoSkinRetouchAction4.main();

// EOF

//"004 AutoSkinRetouch.jsx"
// EOF
