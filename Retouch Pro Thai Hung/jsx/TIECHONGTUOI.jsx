#target photoshop
//
// HONGTUOI.jsx
//

//
// Generated Mon Aug 05 2019 20:22:57 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== HONG TUOI ==============
//
$._ext_TTC023={
run : function HONGTUOI() {
  // Selective Color
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putEnumerated(cTID('Mthd'), cTID('CrcM'), cTID('Rltv'));
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    desc2.putEnumerated(cTID('Clrs'), cTID('Clrs'), cTID('Rds '));
    desc2.putUnitDouble(cTID('Cyn '), cTID('#Prc'), -84);
    list1.putObject(cTID('ClrC'), desc2);
    desc1.putList(cTID('ClrC'), list1);
    executeAction(sTID('selectiveColor'), desc1, dialogMode);
  };

  // Purge
  function step2(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putEnumerated(cTID('null'), cTID('PrgI'), cTID('Al  '));
    executeAction(cTID('Prge'), desc1, dialogMode);
  };

  step1();      // Selective Color
  step2();      // Purge
},
};



//=========================================
//                    HONGTUOI.main
//=========================================
//

//HONGTUOI.main = function () {
 // HONGTUOI();
//};

//HONGTUOI.main();

// EOF

//"HONGTUOI.jsx"
// EOF
