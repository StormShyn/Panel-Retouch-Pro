#target photoshop
//
// Tiec0002.jsx
//

//
// Generated Sun Aug 04 2019 20:56:45 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== Action 1 ==============
//
$._ext_T0002={
run : function Action1() {
  // Convert to Profile
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Dcmn'), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    desc1.putString(cTID('T   '), "sRGB IEC61966-2.1");
    desc1.putEnumerated(cTID('Inte'), cTID('Inte'), cTID('Grp '));
    desc1.putBoolean(cTID('MpBl'), true);
    desc1.putBoolean(cTID('Dthr'), true);
    executeAction(sTID('convertToProfile'), desc1, dialogMode);
  };

  // Assign Profile
  function step2(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Dcmn'), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    desc1.putString(sTID("profile"), "Adobe RGB (1998)");
    executeAction(sTID('assignProfile'), desc1, dialogMode);
  };

  // Convert to Profile
  function step3(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Dcmn'), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    desc1.putString(cTID('T   '), "sRGB IEC61966-2.1");
    desc1.putEnumerated(cTID('Inte'), cTID('Inte'), cTID('Grp '));
    desc1.putBoolean(cTID('MpBl'), true);
    desc1.putBoolean(cTID('Dthr'), true);
    executeAction(sTID('convertToProfile'), desc1, dialogMode);
  };

  // Levels
  function step4(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Cmps'));
    desc2.putReference(cTID('Chnl'), ref1);
    var list2 = new ActionList();
    list2.putInteger(4);
    list2.putInteger(252);
    desc2.putList(cTID('Inpt'), list2);
    desc2.putDouble(cTID('Gmm '), 1.05);
    list1.putObject(cTID('LvlA'), desc2);
    var desc3 = new ActionDescriptor();
    var ref2 = new ActionReference();
    ref2.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Rd  '));
    desc3.putReference(cTID('Chnl'), ref2);
    desc3.putBoolean(cTID('Auto'), true);
    list1.putObject(cTID('LvlA'), desc3);
    var desc4 = new ActionDescriptor();
    var ref3 = new ActionReference();
    ref3.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Grn '));
    desc4.putReference(cTID('Chnl'), ref3);
    desc4.putBoolean(cTID('Auto'), true);
    list1.putObject(cTID('LvlA'), desc4);
    var desc5 = new ActionDescriptor();
    var ref4 = new ActionReference();
    ref4.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Bl  '));
    desc5.putReference(cTID('Chnl'), ref4);
    desc5.putBoolean(cTID('Auto'), true);
    list1.putObject(cTID('LvlA'), desc5);
    desc1.putList(cTID('Adjs'), list1);
    executeAction(cTID('Lvls'), desc1, dialogMode);
  };

  // Unsharp Mask
  function step5(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putUnitDouble(cTID('Amnt'), cTID('#Prc'), 41);
    desc1.putUnitDouble(cTID('Rds '), cTID('#Pxl'), 1);
    desc1.putInteger(cTID('Thsh'), 1);
    executeAction(sTID('unsharpMask'), desc1, dialogMode);
  };

  // Hue/Saturation
  function step6(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putBoolean(cTID('Clrz'), false);
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    desc2.putInteger(cTID('H   '), 0);
    desc2.putInteger(cTID('Strt'), 10);
    desc2.putInteger(cTID('Lght'), 0);
    list1.putObject(cTID('Hst2'), desc2);
    var desc3 = new ActionDescriptor();
    desc3.putInteger(cTID('LclR'), 1);
    desc3.putInteger(cTID('BgnR'), 331);
    desc3.putInteger(cTID('BgnS'), 1);
    desc3.putInteger(cTID('EndS'), 31);
    desc3.putInteger(cTID('EndR'), 61);
    desc3.putInteger(cTID('H   '), 2);
    desc3.putInteger(cTID('Strt'), -6);
    desc3.putInteger(cTID('Lght'), 0);
    list1.putObject(cTID('Hst2'), desc3);
    var desc4 = new ActionDescriptor();
    desc4.putInteger(cTID('LclR'), 2);
    desc4.putInteger(cTID('BgnR'), 318);
    desc4.putInteger(cTID('BgnS'), 348);
    desc4.putInteger(cTID('EndS'), 18);
    desc4.putInteger(cTID('EndR'), 48);
    desc4.putInteger(cTID('H   '), 1);
    desc4.putInteger(cTID('Strt'), -5);
    desc4.putInteger(cTID('Lght'), 0);
    list1.putObject(cTID('Hst2'), desc4);
    var desc5 = new ActionDescriptor();
    desc5.putInteger(cTID('LclR'), 3);
    desc5.putInteger(cTID('BgnR'), 359);
    desc5.putInteger(cTID('BgnS'), 29);
    desc5.putInteger(cTID('EndS'), 59);
    desc5.putInteger(cTID('EndR'), 89);
    desc5.putInteger(cTID('H   '), -4);
    desc5.putInteger(cTID('Strt'), -10);
    desc5.putInteger(cTID('Lght'), 0);
    list1.putObject(cTID('Hst2'), desc5);
    var desc6 = new ActionDescriptor();
    desc6.putInteger(cTID('LclR'), 4);
    desc6.putInteger(cTID('BgnR'), 333);
    desc6.putInteger(cTID('BgnS'), 3);
    desc6.putInteger(cTID('EndS'), 33);
    desc6.putInteger(cTID('EndR'), 63);
    desc6.putInteger(cTID('H   '), -2);
    desc6.putInteger(cTID('Strt'), -8);
    desc6.putInteger(cTID('Lght'), 0);
    list1.putObject(cTID('Hst2'), desc6);
    var desc7 = new ActionDescriptor();
    desc7.putInteger(cTID('LclR'), 5);
    desc7.putInteger(cTID('BgnR'), 130);
    desc7.putInteger(cTID('BgnS'), 160);
    desc7.putInteger(cTID('EndS'), 190);
    desc7.putInteger(cTID('EndR'), 220);
    desc7.putInteger(cTID('H   '), -2);
    desc7.putInteger(cTID('Strt'), -10);
    desc7.putInteger(cTID('Lght'), 20);
    list1.putObject(cTID('Hst2'), desc7);
    var desc8 = new ActionDescriptor();
    desc8.putInteger(cTID('LclR'), 6);
    desc8.putInteger(cTID('BgnR'), 65);
    desc8.putInteger(cTID('BgnS'), 95);
    desc8.putInteger(cTID('EndS'), 125);
    desc8.putInteger(cTID('EndR'), 155);
    desc8.putInteger(cTID('H   '), 8);
    desc8.putInteger(cTID('Strt'), 4);
    desc8.putInteger(cTID('Lght'), 0);
    list1.putObject(cTID('Hst2'), desc8);
    desc1.putList(cTID('Adjs'), list1);
    executeAction(sTID('hueSaturation'), desc1, dialogMode);
  };

  // Hue/Saturation
  function step7(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putBoolean(cTID('Clrz'), false);
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    desc2.putInteger(cTID('LclR'), 2);
    desc2.putInteger(cTID('BgnR'), 325);
    desc2.putInteger(cTID('BgnS'), 355);
    desc2.putInteger(cTID('EndS'), 25);
    desc2.putInteger(cTID('EndR'), 55);
    desc2.putInteger(cTID('H   '), 0);
    desc2.putInteger(cTID('Strt'), -5);
    desc2.putInteger(cTID('Lght'), 0);
    list1.putObject(cTID('Hst2'), desc2);
    var desc3 = new ActionDescriptor();
    desc3.putInteger(cTID('LclR'), 4);
    desc3.putInteger(cTID('BgnR'), 303);
    desc3.putInteger(cTID('BgnS'), 333);
    desc3.putInteger(cTID('EndS'), 3);
    desc3.putInteger(cTID('EndR'), 33);
    desc3.putInteger(cTID('H   '), 1);
    desc3.putInteger(cTID('Strt'), -5);
    desc3.putInteger(cTID('Lght'), -2);
    list1.putObject(cTID('Hst2'), desc3);
    var desc4 = new ActionDescriptor();
    desc4.putInteger(cTID('LclR'), 5);
    desc4.putInteger(cTID('BgnR'), 130);
    desc4.putInteger(cTID('BgnS'), 160);
    desc4.putInteger(cTID('EndS'), 190);
    desc4.putInteger(cTID('EndR'), 220);
    desc4.putInteger(cTID('H   '), 0);
    desc4.putInteger(cTID('Strt'), -10);
    desc4.putInteger(cTID('Lght'), 0);
    list1.putObject(cTID('Hst2'), desc4);
    desc1.putList(cTID('Adjs'), list1);
    executeAction(sTID('hueSaturation'), desc1, dialogMode);
  };

  // Color Range
  function step8(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putInteger(cTID('Fzns'), 90);
    var desc2 = new ActionDescriptor();
    desc2.putDouble(cTID('Lmnc'), 10.61);
    desc2.putDouble(cTID('A   '), -3.59);
    desc2.putDouble(cTID('B   '), -21.19);
    desc1.putObject(cTID('Mnm '), cTID('LbCl'), desc2);
    var desc3 = new ActionDescriptor();
    desc3.putDouble(cTID('Lmnc'), 30.44);
    desc3.putDouble(cTID('A   '), 8.2);
    desc3.putDouble(cTID('B   '), 4.35);
    desc1.putObject(cTID('Mxm '), cTID('LbCl'), desc3);
    desc1.putBoolean(cTID('Invr'), true);
    executeAction(sTID('colorRange'), desc1, dialogMode);
  };

  // Feather
  function step9(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putUnitDouble(cTID('Rds '), cTID('#Pxl'), 5);
    executeAction(cTID('Fthr'), desc1, dialogMode);
  };

  // Inverse
  function step10(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    executeAction(cTID('Invs'), undefined, dialogMode);
  };

  // Layer Via Copy
  function step11(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    executeAction(sTID('copyToLayer'), undefined, dialogMode);
  };

  // Color Balance
  function step12(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var list1 = new ActionList();
    list1.putInteger(0);
    list1.putInteger(0);
    list1.putInteger(0);
    desc1.putList(cTID('ShdL'), list1);
    var list2 = new ActionList();
    list2.putInteger(0);
    list2.putInteger(0);
    list2.putInteger(-5);
    desc1.putList(cTID('MdtL'), list2);
    var list3 = new ActionList();
    list3.putInteger(0);
    list3.putInteger(0);
    list3.putInteger(0);
    desc1.putList(cTID('HghL'), list3);
    desc1.putBoolean(cTID('PrsL'), true);
    executeAction(sTID('colorBalance'), desc1, dialogMode);
  };

  // Selective Color
  function step13(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putEnumerated(cTID('Mthd'), cTID('CrcM'), cTID('Absl'));
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    desc2.putEnumerated(cTID('Clrs'), cTID('Clrs'), cTID('Ntrl'));
    desc2.putUnitDouble(cTID('Cyn '), cTID('#Prc'), -1);
    desc2.putUnitDouble(cTID('Mgnt'), cTID('#Prc'), -2);
    desc2.putUnitDouble(cTID('Ylw '), cTID('#Prc'), 2);
    desc2.putUnitDouble(cTID('Blck'), cTID('#Prc'), -8);
    list1.putObject(cTID('ClrC'), desc2);
    var desc3 = new ActionDescriptor();
    desc3.putEnumerated(cTID('Clrs'), cTID('Clrs'), cTID('Blks'));
    desc3.putUnitDouble(cTID('Blck'), cTID('#Prc'), 2);
    list1.putObject(cTID('ClrC'), desc3);
    desc1.putList(cTID('ClrC'), list1);
    executeAction(sTID('selectiveColor'), desc1, dialogMode);
  };

  // Select
  function step14(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Lyr '), cTID('Bckg'));
    desc1.putReference(cTID('null'), ref1);
    executeAction(cTID('slct'), desc1, dialogMode);
  };

  // Set
  function step15(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putEnumerated(cTID('Md  '), cTID('BlnM'), cTID('SftL'));
    desc1.putObject(cTID('T   '), cTID('Lyr '), desc2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Merge Visible
  function step16(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    executeAction(sTID('mergeVisible'), undefined, dialogMode);
  };

  // Color Balance
  function step17(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var list1 = new ActionList();
    list1.putInteger(0);
    list1.putInteger(0);
    list1.putInteger(0);
    desc1.putList(cTID('ShdL'), list1);
    var list2 = new ActionList();
    list2.putInteger(4);
    list2.putInteger(0);
    list2.putInteger(-10);
    desc1.putList(cTID('MdtL'), list2);
    var list3 = new ActionList();
    list3.putInteger(-1);
    list3.putInteger(-2);
    list3.putInteger(10);
    desc1.putList(cTID('HghL'), list3);
    desc1.putBoolean(cTID('PrsL'), true);
    executeAction(sTID('colorBalance'), desc1, dialogMode);
  };

  // Set
  function step18(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Chnl'), sTID("selection"));
    desc1.putReference(cTID('null'), ref1);
    desc1.putEnumerated(cTID('T   '), cTID('Ordn'), cTID('None'));
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Color Range
  function step19(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putInteger(cTID('Fzns'), 70);
    var desc2 = new ActionDescriptor();
    desc2.putDouble(cTID('Lmnc'), 99.46);
    desc2.putDouble(cTID('A   '), -0.95);
    desc2.putDouble(cTID('B   '), 0.71);
    desc1.putObject(cTID('Mnm '), cTID('LbCl'), desc2);
    var desc3 = new ActionDescriptor();
    desc3.putDouble(cTID('Lmnc'), 99.46);
    desc3.putDouble(cTID('A   '), -0.95);
    desc3.putDouble(cTID('B   '), 0.71);
    desc1.putObject(cTID('Mxm '), cTID('LbCl'), desc3);
    desc1.putBoolean(cTID('Invr'), true);
    executeAction(sTID('colorRange'), desc1, dialogMode);
  };

  // Feather
  function step20(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putUnitDouble(cTID('Rds '), cTID('#Pxl'), 5);
    executeAction(cTID('Fthr'), desc1, dialogMode);
  };

  // Inverse
  function step21(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    executeAction(cTID('Invs'), undefined, dialogMode);
  };

  // Layer Via Copy
  function step22(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    executeAction(sTID('copyToLayer'), undefined, dialogMode);
  };

  // Levels
  function step23(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Cmps'));
    desc2.putReference(cTID('Chnl'), ref1);
    var list2 = new ActionList();
    list2.putInteger(2);
    list2.putInteger(252);
    desc2.putList(cTID('Inpt'), list2);
    desc2.putDouble(cTID('Gmm '), 0.95);
    var list3 = new ActionList();
    list3.putInteger(0);
    list3.putInteger(254);
    desc2.putList(cTID('Otpt'), list3);
    list1.putObject(cTID('LvlA'), desc2);
    var desc3 = new ActionDescriptor();
    var ref2 = new ActionReference();
    ref2.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Rd  '));
    desc3.putReference(cTID('Chnl'), ref2);
    desc3.putBoolean(cTID('Auto'), true);
    desc3.putDouble(cTID('BlcC'), 0.2);
    desc3.putDouble(cTID('WhtC'), 0.2);
    list1.putObject(cTID('LvlA'), desc3);
    var desc4 = new ActionDescriptor();
    var ref3 = new ActionReference();
    ref3.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Grn '));
    desc4.putReference(cTID('Chnl'), ref3);
    desc4.putBoolean(cTID('Auto'), true);
    desc4.putDouble(cTID('BlcC'), 0.2);
    desc4.putDouble(cTID('WhtC'), 0.2);
    list1.putObject(cTID('LvlA'), desc4);
    var desc5 = new ActionDescriptor();
    var ref4 = new ActionReference();
    ref4.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Bl  '));
    desc5.putReference(cTID('Chnl'), ref4);
    desc5.putBoolean(cTID('Auto'), true);
    desc5.putDouble(cTID('BlcC'), 0.2);
    desc5.putDouble(cTID('WhtC'), 0.2);
    list1.putObject(cTID('LvlA'), desc5);
    desc1.putList(cTID('Adjs'), list1);
    executeAction(cTID('Lvls'), desc1, dialogMode);
  };

  // Color Balance
  function step24(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var list1 = new ActionList();
    list1.putInteger(0);
    list1.putInteger(0);
    list1.putInteger(0);
    desc1.putList(cTID('ShdL'), list1);
    var list2 = new ActionList();
    list2.putInteger(0);
    list2.putInteger(0);
    list2.putInteger(0);
    desc1.putList(cTID('MdtL'), list2);
    var list3 = new ActionList();
    list3.putInteger(-2);
    list3.putInteger(0);
    list3.putInteger(2);
    desc1.putList(cTID('HghL'), list3);
    desc1.putBoolean(cTID('PrsL'), true);
    executeAction(sTID('colorBalance'), desc1, dialogMode);
  };

  // Select
  function step25(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Lyr '), cTID('Bckg'));
    desc1.putReference(cTID('null'), ref1);
    executeAction(cTID('slct'), desc1, dialogMode);
  };

  // Brightness/Contrast
  function step26(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putInteger(cTID('Brgh'), -5);
    desc1.putInteger(cTID('Cntr'), -5);
    executeAction(sTID('brightnessEvent'), desc1, dialogMode);
  };

  // Color Range
  function step27(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putInteger(cTID('Fzns'), 80);
    var desc2 = new ActionDescriptor();
    desc2.putDouble(cTID('Lmnc'), 58.53);
    desc2.putDouble(cTID('A   '), 24.8);
    desc2.putDouble(cTID('B   '), 36.23);
    desc1.putObject(cTID('Mnm '), cTID('LbCl'), desc2);
    var desc3 = new ActionDescriptor();
    desc3.putDouble(cTID('Lmnc'), 58.53);
    desc3.putDouble(cTID('A   '), 24.8);
    desc3.putDouble(cTID('B   '), 36.23);
    desc1.putObject(cTID('Mxm '), cTID('LbCl'), desc3);
    desc1.putBoolean(cTID('Invr'), true);
    executeAction(sTID('colorRange'), desc1, dialogMode);
  };

  // Feather
  function step28(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putUnitDouble(cTID('Rds '), cTID('#Pxl'), 5);
    executeAction(cTID('Fthr'), desc1, dialogMode);
  };

  // Inverse
  function step29(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    executeAction(cTID('Invs'), undefined, dialogMode);
  };

  // Shadow/Highlight
  function step30(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var desc2 = new ActionDescriptor();
    desc2.putUnitDouble(cTID('Amnt'), cTID('#Prc'), 10);
    desc2.putUnitDouble(cTID('Wdth'), cTID('#Prc'), 35);
    desc2.putInteger(cTID('Rds '), 135);
    desc1.putObject(cTID('sdwM'), sTID("adaptCorrectTones"), desc2);
    var desc3 = new ActionDescriptor();
    desc3.putUnitDouble(cTID('Amnt'), cTID('#Prc'), 0);
    desc3.putUnitDouble(cTID('Wdth'), cTID('#Prc'), 0);
    desc3.putInteger(cTID('Rds '), 0);
    desc1.putObject(cTID('hglM'), sTID("adaptCorrectTones"), desc3);
    desc1.putDouble(cTID('BlcC'), 0.01);
    desc1.putDouble(cTID('WhtC'), 0.01);
    desc1.putInteger(cTID('Cntr'), 0);
    desc1.putInteger(cTID('ClrC'), 0);
    executeAction(sTID('adaptCorrect'), desc1, dialogMode);
  };

  // Levels
  function step31(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Cmps'));
    desc2.putReference(cTID('Chnl'), ref1);
    var list2 = new ActionList();
    list2.putInteger(10);
    list2.putInteger(255);
    desc2.putList(cTID('Inpt'), list2);
    desc2.putDouble(cTID('Gmm '), 1.3);
    list1.putObject(cTID('LvlA'), desc2);
    desc1.putList(cTID('Adjs'), list1);
    executeAction(cTID('Lvls'), desc1, dialogMode);
  };

  // Selective Color
  function step32(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putEnumerated(cTID('Mthd'), cTID('CrcM'), cTID('Rltv'));
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    desc2.putEnumerated(cTID('Clrs'), cTID('Clrs'), cTID('Ylws'));
    desc2.putUnitDouble(cTID('Cyn '), cTID('#Prc'), 20);
    desc2.putUnitDouble(cTID('Ylw '), cTID('#Prc'), -25);
    list1.putObject(cTID('ClrC'), desc2);
    desc1.putList(cTID('ClrC'), list1);
    executeAction(sTID('selectiveColor'), desc1, dialogMode);
  };

  // Set
  function step33(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Chnl'), sTID("selection"));
    desc1.putReference(cTID('null'), ref1);
    desc1.putEnumerated(cTID('T   '), cTID('Ordn'), cTID('None'));
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Select
  function step34(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Lyr '), cTID('Bckg'));
    desc1.putReference(cTID('null'), ref1);
    executeAction(cTID('slct'), desc1, dialogMode);
  };

  // Unsharp Mask
  function step35(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putUnitDouble(cTID('Amnt'), cTID('#Prc'), 21);
    desc1.putUnitDouble(cTID('Rds '), cTID('#Pxl'), 0.5);
    desc1.putInteger(cTID('Thsh'), 0);
    executeAction(sTID('unsharpMask'), desc1, dialogMode);
  };

  // Color Balance
  function step36(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var list1 = new ActionList();
    list1.putInteger(0);
    list1.putInteger(0);
    list1.putInteger(0);
    desc1.putList(cTID('ShdL'), list1);
    var list2 = new ActionList();
    list2.putInteger(-11);
    list2.putInteger(0);
    list2.putInteger(0);
    desc1.putList(cTID('MdtL'), list2);
    var list3 = new ActionList();
    list3.putInteger(0);
    list3.putInteger(0);
    list3.putInteger(0);
    desc1.putList(cTID('HghL'), list3);
    desc1.putBoolean(cTID('PrsL'), true);
    executeAction(sTID('colorBalance'), desc1, dialogMode);
  };

  // Merge Visible
  function step37(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    executeAction(sTID('mergeVisible'), undefined, dialogMode);
  };

// Merge Visible
  function step38(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    executeAction(sTID('mergeVisible'), undefined, dialogMode);
  };


  step1();      // Convert to Profile
  step2();      // Assign Profile
  step3();      // Convert to Profile
  step4();      // Levels
  step5();      // Unsharp Mask
  step6();      // Hue/Saturation
  step7(false, false);      // Hue/Saturation
  step8();      // Color Range
  step9();      // Feather
  step10();      // Inverse
  step11();      // Layer Via Copy
  step12();      // Color Balance
  step13();      // Selective Color
  step14();      // Select
  step15(false, false);      // Set
  step16(false, false);      // Merge Visible
  step17();      // Color Balance
  step18();      // Set
  step19();      // Color Range
  step20();      // Feather
  step21();      // Inverse
  step22();      // Layer Via Copy
  step23();      // Levels
  step24();      // Color Balance
  step25();      // Select
  step26(false, false);      // Brightness/Contrast
  step27();      // Color Range
  step28();      // Feather
  step29();      // Inverse
  step30(false, false);      // Shadow/Highlight
  step31();      // Levels
  step32();      // Selective Color
  step33();      // Set
  step34();      // Select
  step35();      // Unsharp Mask
  step36();      // Color Balance
  step37(false, false);      // Merge Visible
  step38();      // Merge Visible
},
};



//=========================================
//                    Action1.main
//=========================================
//

//Action1.main = function () {
  //Action1();
//};

//Action1.main();

// EOF

//"Tiec0002.jsx"
// EOF
