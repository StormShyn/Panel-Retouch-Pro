#target photoshop
//
// 002 AutoSkinRetouch.jsx
//

//
// Generated Tue Jul 23 2019 03:25:58 GMT+0700
//

cTID = function (s) { return app.charIDToTypeID(s); };
sTID = function (s) { return app.stringIDToTypeID(s); };

//
//==================== Auto Skin Retouch Action 2 ==============
//
$._ext_002 = {
  run: function AutoSkinRetouchAction2() {
    // Duplicate
    function step1(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      var ref1 = new ActionReference();
      ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
      desc1.putReference(cTID('null'), ref1);
      desc1.putString(cTID('Nm  '), "Smooth Skin");
      desc1.putInteger(cTID('Vrsn'), 5);
      executeAction(cTID('Dplc'), desc1, dialogMode);
    };

    // Convert to Smart Object
    function step2(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      executeAction(sTID('newPlacedLayer'), undefined, dialogMode);
    };

    // Set
    function step3(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      var ref1 = new ActionReference();
      ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
      desc1.putReference(cTID('null'), ref1);
      var desc2 = new ActionDescriptor();
      desc2.putUnitDouble(cTID('Opct'), cTID('#Prc'), 100);
      desc1.putObject(cTID('T   '), cTID('Lyr '), desc2);
      executeAction(cTID('setd'), desc1, dialogMode);
    };

    // Set
    function step4(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      var ref1 = new ActionReference();
      ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
      desc1.putReference(cTID('null'), ref1);
      var desc2 = new ActionDescriptor();
      desc2.putUnitDouble(cTID('Opct'), cTID('#Prc'), 70);
      desc1.putObject(cTID('T   '), cTID('Lyr '), desc2);
      executeAction(cTID('setd'), desc1, dialogMode);
    };

    // Surface Blur
    function step5(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      desc1.putUnitDouble(cTID('Rds '), cTID('#Pxl'), 35);
      desc1.putInteger(cTID('Thsh'), 25);
      executeAction(sTID('surfaceBlur'), desc1, dialogMode);
    };

    // Select
    function step6(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      var ref1 = new ActionReference();
      ref1.putName(cTID('Lyr '), "Background");
      desc1.putReference(cTID('null'), ref1);
      desc1.putBoolean(cTID('MkVs'), false);
      executeAction(cTID('slct'), desc1, dialogMode);
    };

    // Duplicate
    function step7(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      var ref1 = new ActionReference();
      ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
      desc1.putReference(cTID('null'), ref1);
      desc1.putString(cTID('Nm  '), "Extra Smooth Skin");
      desc1.putInteger(cTID('Vrsn'), 5);
      executeAction(cTID('Dplc'), desc1, dialogMode);
    };

    // Move
    function step8(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      var ref1 = new ActionReference();
      ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
      desc1.putReference(cTID('null'), ref1);
      var ref2 = new ActionReference();
      ref2.putIndex(cTID('Lyr '), 3);
      desc1.putReference(cTID('T   '), ref2);
      desc1.putBoolean(cTID('Adjs'), false);
      desc1.putInteger(cTID('Vrsn'), 5);
      executeAction(cTID('move'), desc1, dialogMode);
    };

    // Convert to Smart Object
    function step9(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      executeAction(sTID('newPlacedLayer'), undefined, dialogMode);
    };

    // Gaussian Blur
    function step10(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      desc1.putUnitDouble(cTID('Rds '), cTID('#Pxl'), 24.2);
      executeAction(sTID('gaussianBlur'), desc1, dialogMode);
    };

    // Set
    function step11(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      var ref1 = new ActionReference();
      ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
      desc1.putReference(cTID('null'), ref1);
      var desc2 = new ActionDescriptor();
      desc2.putUnitDouble(cTID('Opct'), cTID('#Prc'), 20);
      desc1.putObject(cTID('T   '), cTID('Lyr '), desc2);
      executeAction(cTID('setd'), desc1, dialogMode);
    };

    // Select
    function step12(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      var ref1 = new ActionReference();
      ref1.putName(cTID('Lyr '), "Background");
      desc1.putReference(cTID('null'), ref1);
      desc1.putBoolean(cTID('MkVs'), false);
      executeAction(cTID('slct'), desc1, dialogMode);
    };

    // Duplicate
    function step13(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      var ref1 = new ActionReference();
      ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
      desc1.putReference(cTID('null'), ref1);
      desc1.putString(cTID('Nm  '), "Details");
      desc1.putInteger(cTID('Vrsn'), 5);
      executeAction(cTID('Dplc'), desc1, dialogMode);
    };

    // Move
    function step14(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      var ref1 = new ActionReference();
      ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
      desc1.putReference(cTID('null'), ref1);
      var ref2 = new ActionReference();
      ref2.putIndex(cTID('Lyr '), 4);
      desc1.putReference(cTID('T   '), ref2);
      desc1.putBoolean(cTID('Adjs'), false);
      desc1.putInteger(cTID('Vrsn'), 5);
      executeAction(cTID('move'), desc1, dialogMode);
    };

    // High Pass
    function step15(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      desc1.putUnitDouble(cTID('Rds '), cTID('#Pxl'), 20);
      executeAction(sTID('highPass'), desc1, dialogMode);
    };

    // Set
    function step16(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      var ref1 = new ActionReference();
      ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
      desc1.putReference(cTID('null'), ref1);
      var desc2 = new ActionDescriptor();
      desc2.putEnumerated(cTID('Md  '), cTID('BlnM'), sTID("linearLight"));
      desc1.putObject(cTID('T   '), cTID('Lyr '), desc2);
      executeAction(cTID('setd'), desc1, dialogMode);
    };

    // Set
    function step17(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      var ref1 = new ActionReference();
      ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
      desc1.putReference(cTID('null'), ref1);
      var desc2 = new ActionDescriptor();
      desc2.putEnumerated(cTID('Md  '), cTID('BlnM'), cTID('SftL'));
      desc1.putObject(cTID('T   '), cTID('Lyr '), desc2);
      executeAction(cTID('setd'), desc1, dialogMode);
    };

    // Set
    function step18(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      var ref1 = new ActionReference();
      ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
      desc1.putReference(cTID('null'), ref1);
      var desc2 = new ActionDescriptor();
      desc2.putEnumerated(cTID('Md  '), cTID('BlnM'), cTID('HrdL'));
      desc1.putObject(cTID('T   '), cTID('Lyr '), desc2);
      executeAction(cTID('setd'), desc1, dialogMode);
    };

    // Set
    function step19(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      var ref1 = new ActionReference();
      ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
      desc1.putReference(cTID('null'), ref1);
      var desc2 = new ActionDescriptor();
      desc2.putEnumerated(cTID('Md  '), cTID('BlnM'), cTID('SftL'));
      desc1.putObject(cTID('T   '), cTID('Lyr '), desc2);
      executeAction(cTID('setd'), desc1, dialogMode);
    };

    // Set
    function step20(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      var ref1 = new ActionReference();
      ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
      desc1.putReference(cTID('null'), ref1);
      var desc2 = new ActionDescriptor();
      desc2.putUnitDouble(cTID('Opct'), cTID('#Prc'), 59);
      desc1.putObject(cTID('T   '), cTID('Lyr '), desc2);
      executeAction(cTID('setd'), desc1, dialogMode);
    };

    // Set
    function step21(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      var ref1 = new ActionReference();
      ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
      desc1.putReference(cTID('null'), ref1);
      var desc2 = new ActionDescriptor();
      desc2.putUnitDouble(cTID('Opct'), cTID('#Prc'), 40);
      desc1.putObject(cTID('T   '), cTID('Lyr '), desc2);
      executeAction(cTID('setd'), desc1, dialogMode);
    };

    // Set
    function step22(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      var ref1 = new ActionReference();
      ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
      desc1.putReference(cTID('null'), ref1);
      var desc2 = new ActionDescriptor();
      desc2.putUnitDouble(cTID('Opct'), cTID('#Prc'), 80);
      desc1.putObject(cTID('T   '), cTID('Lyr '), desc2);
      executeAction(cTID('setd'), desc1, dialogMode);
    };

    // Select
    function step23(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      var ref1 = new ActionReference();
      ref1.putName(cTID('Lyr '), "Smooth Skin");
      desc1.putReference(cTID('null'), ref1);
      desc1.putEnumerated(sTID("selectionModifier"), sTID("selectionModifierType"), sTID("addToSelectionContinuous"));
      desc1.putBoolean(cTID('MkVs'), false);
      executeAction(cTID('slct'), desc1, dialogMode);
    };

    // Make
    function step24(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      var ref1 = new ActionReference();
      ref1.putClass(sTID("layerSection"));
      desc1.putReference(cTID('null'), ref1);
      var ref2 = new ActionReference();
      ref2.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
      desc1.putReference(cTID('From'), ref2);
      executeAction(cTID('Mk  '), desc1, dialogMode);
    };

    // Set
    function step25(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      var ref1 = new ActionReference();
      ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
      desc1.putReference(cTID('null'), ref1);
      var desc2 = new ActionDescriptor();
      desc2.putString(cTID('Nm  '), "Thai Hung KrQ AI 2");
      desc1.putObject(cTID('T   '), cTID('Lyr '), desc2);
      executeAction(cTID('setd'), desc1, dialogMode);
    };

    // Select
    function step26(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      var ref1 = new ActionReference();
      ref1.putName(cTID('Lyr '), "Thai Hung KrQ AI 2");
      desc1.putReference(cTID('null'), ref1);
      desc1.putBoolean(cTID('MkVs'), false);
      executeAction(cTID('slct'), desc1, dialogMode);
    };

    // Make
    function step27(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      desc1.putClass(cTID('Nw  '), cTID('Chnl'));
      var ref1 = new ActionReference();
      ref1.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Msk '));
      desc1.putReference(cTID('At  '), ref1);
      desc1.putEnumerated(cTID('Usng'), cTID('UsrM'), cTID('RvlA'));
      executeAction(cTID('Mk  '), desc1, dialogMode);
    };

    // Select
    function step28(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      var ref1 = new ActionReference();
      ref1.putClass(cTID('PbTl'));
      desc1.putReference(cTID('null'), ref1);
      executeAction(cTID('slct'), desc1, dialogMode);
    };

    // Reset
    function step29(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      var ref1 = new ActionReference();
      ref1.putProperty(cTID('Clr '), cTID('Clrs'));
      desc1.putReference(cTID('null'), ref1);
      executeAction(cTID('Rset'), desc1, dialogMode);
    };

    // Set
    function step30(enabled, withDialog) {
      if (enabled != undefined && !enabled)
        return;
      var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
      var desc1 = new ActionDescriptor();
      var ref1 = new ActionReference();
      ref1.putProperty(cTID('Clr '), cTID('FrgC'));
      desc1.putReference(cTID('null'), ref1);
      var desc2 = new ActionDescriptor();
      desc2.putUnitDouble(cTID('H   '), cTID('#Ang'), 0);
      desc2.putDouble(cTID('Strt'), 6.27450980392157);
      desc2.putDouble(cTID('Brgh'), 0);
      desc1.putObject(cTID('T   '), cTID('HSBC'), desc2);
      executeAction(cTID('setd'), desc1, dialogMode);
    };

    step1();      // Duplicate
    step2();      // Convert to Smart Object
    step3();      // Set
    step4(false, false);      // Set
    step5();      // Surface Blur
    step6();      // Select
    step7();      // Duplicate
    step8();      // Move
    step9();      // Convert to Smart Object
    step10();      // Gaussian Blur
    step11();      // Set
    step12();      // Select
    step13();      // Duplicate
    step14();      // Move
    step15();      // High Pass
    step16();      // Set
    step17();      // Set
    step18();      // Set
    step19();      // Set
    step20();      // Set
    step21();      // Set
    step22();      // Set
    step23();      // Select
    step24();      // Make
    step25();      // Set
    step26();      // Select
    step27();      // Make
    step28();      // Select
    step29();      // Reset
    step30();      // Set
  },
};



//=========================================
//                    AutoSkinRetouchAction2.main
//=========================================
//

//AutoSkinRetouchAction2.main = function () {
 // AutoSkinRetouchAction2();
//};

//AutoSkinRetouchAction2.main();

// EOF

//"002 AutoSkinRetouch.jsx"
// EOF
