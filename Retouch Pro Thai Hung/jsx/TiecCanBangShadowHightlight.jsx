#target photoshop
//
// TiecCanBangShadow_Hightlight.jsx
//

//
// Generated Sun Aug 04 2019 19:18:07 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== Tiec Can Bang Shadow -Hightlight ==============
//
$._ext_TCB={
run : function TiecCanBangShadow_Hightlight() {
  // Shadow/Highlight
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var desc2 = new ActionDescriptor();
    desc2.putUnitDouble(cTID('Amnt'), cTID('#Prc'), 7);
    desc2.putUnitDouble(cTID('Wdth'), cTID('#Prc'), 50);
    desc2.putInteger(cTID('Rds '), 30);
    desc1.putObject(cTID('sdwM'), sTID("adaptCorrectTones"), desc2);
    var desc3 = new ActionDescriptor();
    desc3.putUnitDouble(cTID('Amnt'), cTID('#Prc'), 0);
    desc3.putUnitDouble(cTID('Wdth'), cTID('#Prc'), 50);
    desc3.putInteger(cTID('Rds '), 30);
    desc1.putObject(cTID('hglM'), sTID("adaptCorrectTones"), desc3);
    desc1.putDouble(cTID('BlcC'), 0.01);
    desc1.putDouble(cTID('WhtC'), 0.01);
    desc1.putInteger(cTID('Cntr'), 0);
    desc1.putInteger(cTID('ClrC'), 20);
    executeAction(sTID('adaptCorrect'), desc1, dialogMode);
  };

  step1();      // Shadow/Highlight
},
};



//=========================================
//                    TiecCanBangShadow_Hightlight.main
//=========================================
//

//TiecCanBangShadow_Hightlight.main = function () {
  //TiecCanBangShadow_Hightlight();
//};

//TiecCanBangShadow_Hightlight.main();

// EOF

//"TiecCanBangShadow_Hightlight.jsx"
// EOF
