#target photoshop
//
// TIEC tangnet.jsx
//

//
// Generated Sun Aug 04 2019 20:58:58 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== tang net ==============
//
$._ext_TTN={
run : function tangnet() {
  // Unsharp Mask
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putUnitDouble(cTID('Amnt'), cTID('#Prc'), 67);
    desc1.putUnitDouble(cTID('Rds '), cTID('#Pxl'), 1.9);
    desc1.putInteger(cTID('Thsh'), 0);
    executeAction(sTID('unsharpMask'), desc1, dialogMode);
  };

  step1();      // Unsharp Mask
},
};



//=========================================
//                    tangnet.main
//=========================================
//

//tangnet.main = function () {
 // tangnet();
//};

//tangnet.main();

// EOF

//"TIEC tangnet.jsx"
// EOF
