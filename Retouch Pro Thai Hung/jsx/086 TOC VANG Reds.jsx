#target photoshop
//
// 086 TOC VANG Reds.jsx
//

//
// Generated Wed Jul 31 2019 21:37:31 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== Reds ==============
//
$._ext_086={
run : function Reds() {
  // Make
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putClass(cTID('AdjL'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putString(cTID('Nm  '), "Tuscan");
    desc2.putEnumerated(cTID('Md  '), cTID('BlnM'), cTID('SftL'));
    var desc3 = new ActionDescriptor();
    desc3.putEnumerated(sTID("presetKind"), sTID("presetKindType"), sTID("presetKindCustom"));
    desc3.putBoolean(cTID('Clrz'), true);
    var list1 = new ActionList();
    var desc4 = new ActionDescriptor();
    desc4.putInteger(cTID('H   '), 0);
    desc4.putInteger(cTID('Strt'), 42);
    desc4.putInteger(cTID('Lght'), 49);
    list1.putObject(cTID('Hst2'), desc4);
    desc3.putList(cTID('Adjs'), list1);
    desc2.putObject(cTID('Type'), cTID('HStr'), desc3);
    desc1.putObject(cTID('Usng'), cTID('AdjL'), desc2);
    executeAction(cTID('Mk  '), desc1, dialogMode);
  };

  // Invert
  function step2(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    executeAction(cTID('Invr'), undefined, dialogMode);
  };

  // Make
  function step3(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putClass(cTID('AdjL'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putString(cTID('Nm  '), "Brick");
    desc2.putEnumerated(cTID('Md  '), cTID('BlnM'), cTID('SftL'));
    var desc3 = new ActionDescriptor();
    desc3.putEnumerated(sTID("presetKind"), sTID("presetKindType"), sTID("presetKindCustom"));
    desc3.putBoolean(cTID('Clrz'), true);
    var list1 = new ActionList();
    var desc4 = new ActionDescriptor();
    desc4.putInteger(cTID('H   '), 0);
    desc4.putInteger(cTID('Strt'), 100);
    desc4.putInteger(cTID('Lght'), 27);
    list1.putObject(cTID('Hst2'), desc4);
    desc3.putList(cTID('Adjs'), list1);
    desc2.putObject(cTID('Type'), cTID('HStr'), desc3);
    desc1.putObject(cTID('Usng'), cTID('AdjL'), desc2);
    executeAction(cTID('Mk  '), desc1, dialogMode);
  };

  // Invert
  function step4(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    executeAction(cTID('Invr'), undefined, dialogMode);
  };

  // Make
  function step5(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putClass(cTID('AdjL'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putString(cTID('Nm  '), "Candy Apple");
    desc2.putUnitDouble(cTID('Opct'), cTID('#Prc'), 70);
    desc2.putEnumerated(cTID('Md  '), cTID('BlnM'), cTID('Clr '));
    var desc3 = new ActionDescriptor();
    desc3.putEnumerated(sTID("presetKind"), sTID("presetKindType"), sTID("presetKindCustom"));
    desc3.putBoolean(cTID('Clrz'), true);
    var list1 = new ActionList();
    var desc4 = new ActionDescriptor();
    desc4.putInteger(cTID('H   '), 360);
    desc4.putInteger(cTID('Strt'), 71);
    desc4.putInteger(cTID('Lght'), 27);
    list1.putObject(cTID('Hst2'), desc4);
    desc3.putList(cTID('Adjs'), list1);
    desc2.putObject(cTID('Type'), cTID('HStr'), desc3);
    desc1.putObject(cTID('Usng'), cTID('AdjL'), desc2);
    executeAction(cTID('Mk  '), desc1, dialogMode);
  };

  // Invert
  function step6(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    executeAction(cTID('Invr'), undefined, dialogMode);
  };

  // Make
  function step7(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putClass(cTID('AdjL'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putString(cTID('Nm  '), "Ron Burgandy");
    desc2.putEnumerated(cTID('Md  '), cTID('BlnM'), cTID('SftL'));
    var desc3 = new ActionDescriptor();
    desc3.putEnumerated(sTID("presetKind"), sTID("presetKindType"), sTID("presetKindCustom"));
    desc3.putBoolean(cTID('Clrz'), true);
    var list1 = new ActionList();
    var desc4 = new ActionDescriptor();
    desc4.putInteger(cTID('H   '), 342);
    desc4.putInteger(cTID('Strt'), 66);
    desc4.putInteger(cTID('Lght'), 27);
    list1.putObject(cTID('Hst2'), desc4);
    desc3.putList(cTID('Adjs'), list1);
    desc2.putObject(cTID('Type'), cTID('HStr'), desc3);
    desc1.putObject(cTID('Usng'), cTID('AdjL'), desc2);
    executeAction(cTID('Mk  '), desc1, dialogMode);
  };

  // Invert
  function step8(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    executeAction(cTID('Invr'), undefined, dialogMode);
  };

  // Make
  function step9(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putClass(cTID('AdjL'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putString(cTID('Nm  '), "Rustic");
    desc2.putEnumerated(cTID('Md  '), cTID('BlnM'), cTID('Clr '));
    var desc3 = new ActionDescriptor();
    desc3.putEnumerated(sTID("presetKind"), sTID("presetKindType"), sTID("presetKindCustom"));
    desc3.putBoolean(cTID('Clrz'), true);
    var list1 = new ActionList();
    var desc4 = new ActionDescriptor();
    desc4.putInteger(cTID('H   '), 18);
    desc4.putInteger(cTID('Strt'), 92);
    desc4.putInteger(cTID('Lght'), 59);
    list1.putObject(cTID('Hst2'), desc4);
    desc3.putList(cTID('Adjs'), list1);
    desc2.putObject(cTID('Type'), cTID('HStr'), desc3);
    desc1.putObject(cTID('Usng'), cTID('AdjL'), desc2);
    executeAction(cTID('Mk  '), desc1, dialogMode);
  };

  // Invert
  function step10(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    executeAction(cTID('Invr'), undefined, dialogMode);
  };

  step1();      // Make
  step2();      // Invert
  step3();      // Make
  step4();      // Invert
  step5();      // Make
  step6();      // Invert
  step7();      // Make
  step8();      // Invert
  step9();      // Make
  step10();      // Invert
},
};



//=========================================
//                    Reds.main
//=========================================
//

//Reds.main = function () {
 // Reds();
//};

//Reds.main();

// EOF

//"086 TOC VANG Reds.jsx"
// EOF
