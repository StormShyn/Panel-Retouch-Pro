var matpudratext, cilleketext, ciltyumusattext, puruzsuzcilttext, maskaratext, parlaticipudratext, gozpartext, alliktext, fartext, dudakbelirgintext, ciltaydintext, disbeyaztext, dodgeburntext, kontrasttext, keskintext, detayguclentext, storebutton, trbutton, enbutton, aboutbutton, notifisbutton, notifmainbutton, isTr = 0,
 isNotif = 1,
 matbutton, cilbutton, ciltyumusatbutton, puruzsuzciltbutton, maskarabutton, parpudrabutton, gozparlaticibutton, allikbutton, farbutton, dudakbelirbutton, ciltaydinbutton, disbeyazbutton, dodgebutton, kontrastbutton,
 keskinbutton, detaybutton, perfectbutton, carrotbutton, gozenekbutton, rednessbutton, perfecttext, carrottext, gozenektext, rednesstext, fiftygreytext;

function onLoaded() {
 var a = new CSInterface;
 "FLPR" != a.hostEnvironment.appName && loadJSX();
 updateThemeWithAppSkinInfo(a.hostEnvironment.appSkinInfo);
 a.addEventListener(CSInterface.THEME_COLOR_CHANGED_EVENT, onAppThemeColorChanged);
 if (fiftygreytext = document.getElementById("50greyt")) fiftygreytext.disabled = !1;
 if (perfectbutton = document.getElementById("perfect-skin")) perfectbutton.disabled = !1;
 if (carrotbutton = document.getElementById("havucb")) carrotbutton.disabled = !1;
 if (gozenekbutton = document.getElementById("poretightb")) gozenekbutton.disabled = !1;
 if (rednessbutton = document.getElementById("rednessb")) rednessbutton.disabled = !1;
 if (perfecttext = document.getElementById("kusursuzciltt")) perfecttext.disabled = !1;
 if (carrottext = document.getElementById("havuct")) carrottext.disabled = !1;
 if (gozenektext = document.getElementById("poretightt")) gozenektext.disabled = !1;
 if (rednesstext = document.getElementById("rednesst")) rednesstext.disabled = !1;
 if (matpudratext = document.getElementById("matpudra")) matpudratext.disabled = !1;
 if (cilleketext = document.getElementById("cilleket")) cilleketext.disabled = !1;
 if (ciltyumusattext = document.getElementById("ciltyumust")) ciltyumusattext.disabled = !1;
 if (puruzsuzcilttext = document.getElementById("puruzsuzciltt")) puruzsuzcilttext.disabled = !1;
 if (maskaratext = document.getElementById("maskarat")) maskaratext.disabled = !1;
 if (parlaticipudratext = document.getElementById("parpudt")) parlaticipudratext.disabled = !1;
 if (gozpartext = document.getElementById("gozpart")) gozpartext.disabled = !1;
 if (alliktext = document.getElementById("allikt")) alliktext.disabled = !1;
 if (fartext = document.getElementById("fart")) fartext.disabled = !1;
 if (dudakbelirgintext = document.getElementById("dudakbelirt")) dudakbelirgintext.disabled = !1;
 if (ciltaydintext = document.getElementById("ciltaydint")) ciltaydintext.disabled = !1;
 if (disbeyaztext = document.getElementById("disbeyazt")) disbeyaztext.disabled = !1;
 if (dodgeburntext = document.getElementById("dodget")) dodgeburntext.disabled = !1;
 if (kontrasttext = document.getElementById("kontrastt")) kontrasttext.disabled = !1;
 if (keskintext = document.getElementById("sharpent")) keskintext.disabled = !1;
 if (detayguclentext = document.getElementById("detay")) detayguclentext.disabled = !1;
 if (storebutton = document.getElementById("store")) storebutton.disabled = !1;
 if (trbutton = document.getElementById("tr")) trbutton.disabled = !1;
 if (enbutton = document.getElementById("en")) enbutton.disabled = !1;
 if (aboutbutton = document.getElementById("about")) aboutbutton.disabled = !1;
 if (notifisbutton = document.getElementById("notif")) notifisbutton.disabled = !1;
 if (notifmainbutton = document.getElementById("notifmain")) notifmainbutton.disabled = !1;
 if (matbutton = document.getElementById("matpudrab")) matbutton.disabled = !1;
 if (cilbutton = document.getElementById("cillekeb")) cilbutton.disabled = !1;
 if (ciltyumusatbutton = document.getElementById("ciltyumusb")) ciltyumusatbutton.disabled = !1;
 if (puruzsuzciltbutton = document.getElementById("ultra-smooth-skin")) puruzsuzciltbutton.disabled = !1;
 if (maskarabutton = document.getElementById("maskarab")) maskarabutton.disabled = !1;
 if (parpudrabutton = document.getElementById("parpudb")) parpudrabutton.disabled = !1;
 if (gozparlaticibutton = document.getElementById("eye-brighten")) gozparlaticibutton.disabled = !1;
 if (allikbutton = document.getElementById("allikb")) allikbutton.disabled = !1;
 if (farbutton = document.getElementById("farb")) farbutton.disabled = !1;
 if (dudakbelirbutton = document.getElementById("dudakbelirb")) dudakbelirbutton.disabled = !1;
 if (ciltaydinbutton = document.getElementById("ciltaydinb")) ciltaydinbutton.disabled = !1;
 if (disbeyazbutton = document.getElementById("disbeyazb")) disbeyazbutton.disabled = !1;
 if (dodgebutton = document.getElementById("dodgeb")) dodgebutton.disabled = !1;
 if (kontrastbutton = document.getElementById("kontrastb")) kontrastbutton.disabled = !1;
 if (keskinbutton = document.getElementById("keskinb")) keskinbutton.disabled = !1;
 if (detaybutton = document.getElementById("detayb")) detaybutton.disabled = !1;
 trbutton.onclick = function() {
  isTr = 1;
  matpudratext.innerHTML = "Matla\u015ft\u0131r\u0131c\u0131 Pudra";
  cilleketext.innerHTML = "\u00c7il ve Leke Azalt\u0131c\u0131";
  ciltyumusattext.innerHTML = "Cilt Yumu\u015fat\u0131c\u0131";
  puruzsuzcilttext.innerHTML = "P\u00fcr\u00fczs\u00fcz Cilt";
  maskaratext.innerHTML = "Maskara";
  parlaticipudratext.innerHTML = "Parlat\u0131c\u0131 Pudra";
  gozpartext.innerHTML = "G\u00f6z Parlat\u0131c\u0131";
  alliktext.innerHTML = "All\u0131k";
  fartext.innerHTML = "Far";
  dudakbelirgintext.innerHTML = "Dudak Belirginle\u015ftirici";
  ciltaydintext.innerHTML = "Cilt Ayd\u0131nlat\u0131c\u0131";
  disbeyaztext.innerHTML = "Di\u015f Beyazlat\u0131c\u0131";
  dodgeburntext.innerHTML = "Dodge&Burn";
  kontrasttext.innerHTML = "Renk Kontrast\u0131";
  keskintext.innerHTML = "Keskinle\u015ftirici";
  detayguclentext.innerHTML = "Detay G\u00fc\u00e7lendirici";
  perfecttext.innerHTML = "Kusursuz Cilt";
  carrottext.innerHTML = "Havu\u00e7 Ya\u011f\u0131";
  rednesstext.innerHTML = "K\u0131zar\u0131kl\u0131k Azalt\u0131c\u0131";
  gozenektext.innerHTML = "G\u00f6zenek S\u0131k\u0131la\u015ft\u0131r\u0131c\u0131";
  fiftygreytext.innerHTML = "%50 Gri";
  storebutton.innerHTML = "ma\u011fazam\u0131z";
  aboutbutton.innerHTML = "hakk\u0131m\u0131zda";
  notifmainbutton.innerHTML = "bildirim";
  1 == isNotif ? (notifisbutton.innerHTML = "a\u00e7\u0131k", trExecute()) : 0 == isNotif && (notifisbutton.innerHTML = "kapal\u0131")
 };
 enbutton.onclick = function() {
  isTr =
   0;
  matpudratext.innerHTML = "Mattifying Powder";
  cilleketext.innerHTML = "Freckle and Stain Reducer";
  ciltyumusattext.innerHTML = "Skin Softener";
  puruzsuzcilttext.innerHTML = "Ultra Smooth Skin";
  maskaratext.innerHTML = "Mascara";
  parlaticipudratext.innerHTML = "Highlight Powder";
  gozpartext.innerHTML = "Eye Brightener";
  alliktext.innerHTML = "Blusher";
  fartext.innerHTML = "Eyeshadow";
  dudakbelirgintext.innerHTML = "Lip Enhancer";
  ciltaydintext.innerHTML = "Skin Brightener";
  disbeyaztext.innerHTML = "Teeth Whitener";
  dodgeburntext.innerHTML =
   "Dodge&Burn";
  kontrasttext.innerHTML = "Contrast";
  keskintext.innerHTML = "Sharpen";
  detayguclentext.innerHTML = "Detail Enhancer";
  perfecttext.innerHTML = "Perfect Skin";
  carrottext.innerHTML = "Carrot Oil";
  rednesstext.innerHTML = "Redness Reducer";
  gozenektext.innerHTML = "Pore Tightening";
  fiftygreytext.innerHTML = "%50 Grey";
  storebutton.innerHTML = "store";
  aboutbutton.innerHTML = "about";
  notifmainbutton.innerHTML = "notification";
  1 == isNotif ? (notifisbutton.innerHTML = "on", enExecute()) : 0 == isNotif && (notifisbutton.innerHTML = "off")
 };
 notifisbutton.onclick = function() {
  1 == isNotif ? (isNotif = 0, 1 == isTr ? notifisbutton.innerHTML = "kapal\u0131" : 0 == isTr && (notifisbutton.innerHTML = "off"), notifExecute()) : 0 == isNotif && (isNotif = 1, 1 == isTr ? notifisbutton.innerHTML = "a\u00e7\u0131k" : 0 == isTr && (notifisbutton.innerHTML = "on"), 1 == isTr ? trExecute() : 0 == isTr && enExecute())
 }
}

function updateThemeWithAppSkinInfo(a) {
 var b = a.panelBackgroundColor.color;
 document.body.bgColor = toHex(b);
 var c = (new CSInterface).hostEnvironment.appName;
 "PHXS" == c && addRule("ppstyle", "button, select, input[type=button], input[type=submit]", "border-radius:3px;");
 if ("PHXS" == c || "PPRO" == c || "PRLD" == c) {
  var c = "background-image: -webkit-linear-gradient(top, " + toHex(b, 40) + " , " + toHex(b, 10) + ");",
   d = "background-image: -webkit-linear-gradient(top, " + toHex(b, 15) + " , " + toHex(b, 5) + ");",
   e, f, g, h, k;
  127 < b.red ? (e = "#000000;",
   f = "color:" + toHex(b, -70) + ";", g = "border-color: " + toHex(b, -90) + ";", h = toHex(b, 54) + ";", k = "background-image: -webkit-linear-gradient(top, " + toHex(b, -40) + " , " + toHex(b, -50) + ");") : (e = "#ffffff;", f = "color:" + toHex(b, 100) + ";", g = "border-color: " + toHex(b, -45) + ";", h = toHex(b, -20) + ";", k = "background-image: -webkit-linear-gradient(top, " + toHex(b, -20) + " , " + toHex(b, -30) + ");");
  addRule("ppstyle", ".default", "font-size:" + a.baseFontSize + "px; color:" + e + "; background-color:" + toHex(b) + ";");
  addRule("ppstyle", "button, select, input[type=text], input[type=button], input[type=submit]",
   g);
  addRule("ppstyle", "button, select, input[type=button], input[type=submit]", c);
  addRule("ppstyle", "button, select, input[type=button], input[type=submit]", "-webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.4), 0 1px 1px rgba(0, 0, 0, 0.2);");
  addRule("ppstyle", "button:enabled:active, input[type=button]:enabled:active, input[type=submit]:enabled:active", k);
  addRule("ppstyle", "button:enabled:active, input[type=button]:enabled:active, input[type=submit]:enabled:active", "-webkit-box-shadow: inset 0 1px 4px rgba(0, 0, 0, 0.6);");
  addRule("ppstyle", "[disabled]", d);
  addRule("ppstyle", "[disabled]", f);
  addRule("ppstyle", "input[type=text]", "padding:1px 3px;");
  addRule("ppstyle", "input[type=text]", "background-color: " + h) + ";";
  addRule("ppstyle", "input[type=text]:focus", "background-color: #ffffff;");
  addRule("ppstyle", "input[type=text]:focus", "color: #000000;")
 } else addRule("ppstyle", ".default", "font-size:" + a.baseFontSize + "px; color:" + reverseColor(b) + "; background-color:" + toHex(b, 20)), addRule("ppstyle", "button", "border-color: " + toHex(panelBgColor,
  -50))
}

function addRule(a, b, c) {
 if (a = document.getElementById(a)) a = a.sheet, a.addRule ? a.addRule(b, c) : a.insertRule && a.insertRule(b + " { " + c + " }", a.cssRules.length)
}

function reverseColor(a, b) {
 return toHex({
  red: Math.abs(255 - a.red),
  green: Math.abs(255 - a.green),
  blue: Math.abs(255 - a.blue)
 }, b)
}

function toHex(a, b) {
 function c(a, b) {
  var c = !isNaN(b) ? a + b : a;
  0 > c ? c = 0 : 255 < c && (c = 255);
  c = c.toString(16);
  return 1 == c.length ? "0" + c : c
 }
 var d = "";
 if (a) with(a) d = c(red, b) + c(green, b) + c(blue, b);
 return "#" + d
}

function onAppThemeColorChanged(a) {
 a = JSON.parse(window.__adobe_cep__.getHostEnvironment()).appSkinInfo;
 updateThemeWithAppSkinInfo(a)
}

function loadJSX() {
 var a = new CSInterface,
  b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/";
 a.evalScript('$._ext.evalFiles("' + b + '")')
}

function evalScript(a, b) {
 (new CSInterface).evalScript(a, b)
}

function onClickButton(a) {
 evalScript("$._ext_" + a + ".run()")
}

function OpenInNewTab() {
 (new CSInterface).openURLInDefaultBrowser("https://www.facebook.com/phu.art.studio/")
}

function enExecute() {
 matbutton.onclick = function() {
  onClickButton("002")
 };
 cilbutton.onclick = function() {
  onClickButton("003")
 };
 ciltyumusatbutton.onclick = function() {
  onClickButton("004")
 };
 puruzsuzciltbutton.onclick = function() {
  onClickButton("005")
 };
 maskarabutton.onclick = function() {
  onClickButton("007")
 };
 parpudrabutton.onclick = function() {
  onClickButton("006")
 };
 gozparlaticibutton.onclick = function() {
  onClickButton("010")
 };
 allikbutton.onclick = function() {
  onClickButton("008")
 };
 farbutton.onclick = function() {
  onClickButton("009")
 };
 dudakbelirbutton.onclick = function() {
  onClickButton("012")
 };
 ciltaydinbutton.onclick = function() {
  onClickButton("011")
 };
 disbeyazbutton.onclick = function() {
  onClickButton("013")
 };
 dodgebutton.onclick = function() {
  onClickButton("014")
 };
 kontrastbutton.onclick = function() {
  onClickButton("015")
 };
 keskinbutton.onclick = function() {
  onClickButton("018")
 };
 detaybutton.onclick = function() {
  onClickButton("017")
 };
 perfectbutton.onclick = function() {
  onClickButton("perfect")
 };
 carrotbutton.onclick = function() {
  onClickButton("carrot")
 };
 rednessbutton.onclick = function() {
  onClickButton("redness")
 };
 gozenekbutton.onclick = function() {
  onClickButton("pore")
 }
}

function trExecute() {
 matbutton.onclick = function() {
  onClickButton("matpudra")
 };
 cilbutton.onclick = function() {
  onClickButton("cilveleke")
 };
 ciltyumusatbutton.onclick = function() {
  onClickButton("ciltyumusatici")
 };
 puruzsuzciltbutton.onclick = function() {
  onClickButton("puruzsuzcilt")
 };
 maskarabutton.onclick = function() {
  onClickButton("maskara")
 };
 parpudrabutton.onclick = function() {
  onClickButton("parlaticipudra")
 };
 gozparlaticibutton.onclick = function() {
  onClickButton("gozparlatici")
 };
 allikbutton.onclick = function() {
  onClickButton("allik")
 };
 farbutton.onclick = function() {
  onClickButton("far")
 };
 dudakbelirbutton.onclick = function() {
  onClickButton("dudakbelirgin")
 };
 ciltaydinbutton.onclick = function() {
  onClickButton("ciltaydinlatici")
 };
 disbeyazbutton.onclick = function() {
  onClickButton("disbeyaz")
 };
 dodgebutton.onclick = function() {
  onClickButton("014")
 };
 kontrastbutton.onclick = function() {
  onClickButton("renkkontrast")
 };
 keskinbutton.onclick = function() {
  onClickButton("keskinlestirici")
 };
 detaybutton.onclick = function() {
  onClickButton("detayguclendir")
 };
 perfectbutton.onclick =
  function() {
   onClickButton("kusursuz")
  };
 carrotbutton.onclick = function() {
  onClickButton("havuc")
 };
 rednessbutton.onclick = function() {
  onClickButton("kizariklik")
 };
 gozenekbutton.onclick = function() {
  onClickButton("gozenek")
 }
}

function notifExecute() {
 matbutton.onclick = function() {
  onClickButton("nomatpudra")
 };
 cilbutton.onclick = function() {
  onClickButton("nocilveleke")
 };
 ciltyumusatbutton.onclick = function() {
  onClickButton("nociltyumusatici")
 };
 puruzsuzciltbutton.onclick = function() {
  onClickButton("nopuruzsuzcilt")
 };
 maskarabutton.onclick = function() {
  onClickButton("nomaskara")
 };
 parpudrabutton.onclick = function() {
  onClickButton("noparlaticipudra")
 };
 gozparlaticibutton.onclick = function() {
  onClickButton("nogozparlatici")
 };
 allikbutton.onclick =
  function() {
   onClickButton("noallik")
  };
 farbutton.onclick = function() {
  onClickButton("nofar")
 };
 dudakbelirbutton.onclick = function() {
  onClickButton("nodudakbelirgin")
 };
 ciltaydinbutton.onclick = function() {
  onClickButton("nociltaydinlatici")
 };
 disbeyazbutton.onclick = function() {
  onClickButton("nodisbeyaz")
 };
 dodgebutton.onclick = function() {
  onClickButton("014")
 };
 kontrastbutton.onclick = function() {
  onClickButton("norenkkontrast")
 };
 keskinbutton.onclick = function() {
  onClickButton("nokeskinlestirici")
 };
 detaybutton.onclick =
  function() {
   onClickButton("nodetayguclendir")
  };
 perfectbutton.onclick = function() {
  onClickButton("nokusursuz")
 };
 carrotbutton.onclick = function() {
  onClickButton("nohavuc")
 };
 rednessbutton.onclick = function() {
  onClickButton("nokizariklik")
 };
 gozenekbutton.onclick = function() {
  onClickButton("nogozenek")
 }
};
if(typeof($)=='undefined')
	$={};
function urlopen(){cep.util.openURLInDefaultBrowser("https://www.facebook.com/phu.art.studio/")};
